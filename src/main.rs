use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: {} <input_string>", args[0]);
        process::exit(1);
    }

    let input_data = &args[1];
    let processed_data = process_data(input_data);

    println!("Processed data: {}", processed_data.0);
    println!("Letter count: {}", processed_data.1);
    println!("Size in bytes: {}", processed_data.2);
}

fn process_data(data: &str) -> (String, usize, usize) {
    let reversed_data = data.chars().rev().collect::<String>();
   
    let letter_count = data.chars().filter(|c| c.is_ascii_alphabetic()).count();

    let size_in_bytes = data.as_bytes().len();

    (reversed_data, letter_count, size_in_bytes)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_data() {
        let result = process_data("good job");
        assert_eq!(result, ("boj doog".to_string(),7,8));
        
        let result = process_data("hello, world!");
        assert_eq!(result, ("!dlrow ,olleh".to_string(),10,13));
    }
}
