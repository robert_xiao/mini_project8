# mini_project8


## Overview:
This project showcases the utilization of Rust functions in a command-line tool. It involves automated testing of the project's functions. The primary function of the command-line tool is to take a string as input, invert the output, and provide the number of letters and byte size of the string.<br>
## Steps:
1.	Use the following code to create a new rust project:<br>
 ![Alt text](image.png)
2.	Write the code shown below in the main.rs file. And the function's main goal is to take a string as input, invert it, and output the string's character count and byte size. <br>
 ![Alt text](image-1.png)<br>
 ![Alt text](image-2.png)<br>
3.	After implementing the program, we can test the tool's functionality by running it with the following command.<br>
 ![Alt text](image-3.png)<br>
The output looks like the following.<br>
 ![Alt text](image-4.png)<br>
4.	We can add automated tests in the code that looks like this.<br>

![Alt text](image-5.png)<br>
5.	In this test function, we have two test cases with inputs "boj doog",7,8 and "!dlrow ,olleh",10,13. We can then run the command.<br>
 ![Alt text](image-6.png)<br>
6.	The test result is shown in the following screenshot:<br>
 ![Alt text](image-7.png)<br>
test reports "ok", and 1 tests passed and no test failed.<br>

